/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : zhang618

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-10-31 18:19:46
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_system_config`
-- ----------------------------
DROP TABLE IF EXISTS `db_system_config`;
CREATE TABLE `db_system_config` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_value` varchar(200) DEFAULT NULL COMMENT '配置值',
  `class_id` int(11) DEFAULT NULL COMMENT '所属分类',
  `createa_time` varchar(20) DEFAULT NULL,
  `update_time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_system_config
-- ----------------------------
