<?php
namespace Home\Controller;


use Common\Tool\Tool;

//前台模块
class IndexController extends BaseController
{
	//首页
	public function index()
	{
    	if (Tool::isMobile()) {
		    header('Location:'.U('Mobile/Index/index'));
    	}
		$ad = new \Home\Model\AdModel();
		
		//首页大分类
		$result_banner = $ad->select(array(
		    'where' => array('ad_space_id' => 1),
		    'field' => array('ad_link', 'pic_url'),
		    'limit' => 6
		), new \Think\Model('goods'));
		
        //首页 分类商品数据
        if (!S('product'))
        {
            $model = new \Home\Model\GoodsClassModel();
    		$data = $model->select(array(
    		    'where' => array('fid' => 0, 'hide_status' => 0, 'shoutui' => 1, 'type' => 1),
    		    'field' => array('id', 'class_name', 'fid', 'description'),
    		    'limit' => 6
    		), new \Think\Model('goods'));
    		S('product', $data, 10);
        }
        
		//\ueditor\php\upload
		//首页banner图下面的广告
		if (!S('ad_space'))
		{
            $ad_space_model = new \Home\Model\AdSpaceModel();
    		
    		$ads            = $ad_space_model->select(array(
    		    'where' => array('type' => array('in','1,3,4')),
    		    'field' => array('id', 'name'),
    		    'order' => array('create_time' => 'DESC', 'update_time' => 'DESC'),
    		    'limt'  => 3,
    		), new \Think\Model('ad'));
		    S('ad_space', $ads, 20);
		}
		$m = M('article');
		
// 		$resul_article = $m->order('id DESC')->limit(7)->order('sort_num')->select();
// 		$this->assign('resul_article',$resul_article);

		
		$result_gonggao = M('page')->where('id=3')->find();
		
		$this->assign('result_gonggao',$result_gonggao);
		$this->assign('result_banner',$result_banner);
		$this->assist();
        $this->product = S('product');
        $this->ads     = S('ad_space');
		$this->display();
	}

	/**
	 * 辅助 死函数 
	 */
	private function assist()
	{
	    $m = M('ad');
	    $result_banner = $m->where('ad_space_id=1')->select();
	    $this->assign('result_banner',$result_banner);
	    

	   
	    $result_ad3 = $m->where('ad_space_id=3')->limit(3)->select();
	    $this->assign('ad_2',$result_ad3);
	    $result_ad4 = $m->where('ad_space_id=4')->limit(3)->select();
	    $this->assign('ad_4',$result_ad4);

	
	    $result_ad28 = $m->where('ad_space_id=28')->limit(1)->select();
	    $this->assign('result_ad28',$result_ad28);
	    
	    $result_ad_home = $m->where('id in (71,72,73,74,75)')->limit(3)->select();
	    $this->assign('ad_6',$result_ad_home);

	}
	
	
	//显示二维码 和他说明
	public  function  qr_code_show(){
		$this->display();
	}
	
	//新闻列表
	public function article(){
    	$m = M('article');
    	$nowPage = isset($_GET['p'])?$_GET['p']:1;
    	// page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
    	$result = $m->where($where)->order('sort_num ASC')->page($nowPage.','.PAGE_SIZE)->select();
    	foreach ($result as $k=>$v){
    		$result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
    	}
    	$this->assign('result',$result);
		
		$info = M('Single');
		$list_1 = $info->field('id,single_title,type')->where('type="新手指南"')->select();
		$this->assign('list_1',$list_1);
		
		$list_2 = $info->field('id,single_title,type')->where('type="购物指南"')->select();
		$this->assign('list_2',$list_2);
		
		$list_3 = $info->field('id,single_title,type')->where('type="支付方式"')->select();
		$this->assign('list_3',$list_3);
		
		$list_4 = $info->field('id,single_title,type')->where('type="客服中心"')->select();
		$this->assign('list_4',$list_4);
    	
    	//分页
    	$count = $m->where($where)->count(id);		// 查询满足要求的总记录数
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	$this->assign('page',$show);// 赋值分页输出   	
    	$this->display();
	}
	
	//新闻详情
	public function article_details(){
		$m = M('article');
		$result = $m->where('id='.$_GET['id'])->find();
		$result['create_time'] = date('Y-m-d',$result['create_time']);
		$this->assign('result',$result);
		
		$info = M('Single');
		$list_1 = $info->field('id,single_title,type')->where('type="新手指南"')->select();
		$this->assign('list_1',$list_1);
		
		$list_2 = $info->field('id,single_title,type')->where('type="购物指南"')->select();
		$this->assign('list_2',$list_2);
		
		$list_3 = $info->field('id,single_title,type')->where('type="支付方式"')->select();
		$this->assign('list_3',$list_3);
		
		$list_4 = $info->field('id,single_title,type')->where('type="客服中心"')->select();
		$this->assign('list_4',$list_4);
		
		$this->display();
	}
	
	
	
	public function goods2(){	
		$m = M('goods_class');
		$where3['fid'] = 0;
		$result_class = $m->where($where3)->select();
		$this->assign('result_class',$result_class);

    	$m = M('goods');
    	$nowPage = isset($_GET['p'])?$_GET['p']:1;
		if($_GET['class_id']){
			$where['fid'] = $_GET['class_id'];
		}
		$where['shangjia'] = 1;
		//dump($where);
    	// page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
    	$result = $m->where($where)->order('sort_num ASC')->page($nowPage.','.PAGE_SIZE)->select();
		
		
		//echo $m->getlastsql();
		
    	foreach ($result as $k=>$v){
    		$result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
    	}

    	$this->assign('result',$result);
		
		$info = M('Single');
		$list_1 = $info->field('id,single_title,type')->where('type="新手指南"')->select();
		$this->assign('list_1',$list_1);
		
		$list_2 = $info->field('id,single_title,type')->where('type="购物指南"')->select();
		$this->assign('list_2',$list_2);
		
		$list_3 = $info->field('id,single_title,type')->where('type="支付方式"')->select();
		$this->assign('list_3',$list_3);
		
		$list_4 = $info->field('id,single_title,type')->where('type="客服中心"')->select();
		$this->assign('list_4',$list_4);
    	
    	//分页
    	$count = $m->where($where)->count(id);		// 查询满足要求的总记录数
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	$this->assign('page',$show);// 赋值分页输出   	
    	$this->display();
	}
	
	
	//商品详情
	public function lvyou_details(){
		$m = M('goods');
		$result = $m->where('id='.$_GET['id'])->find();
		$result['create_time'] = date('Y-m-d',$result['create_time']);
		$this->assign('result',$result);

		$chufa_taocan = unserialize($result['chufa_taocan']);
		$this->assign('chufa_taocan',$chufa_taocan);
		//dump($chufa_taocan);

		foreach($chufa_taocan as $k1 => $v1){
			$chufa_date_str .= "'".$v1['chufa_date']."',";
		}
		$this->assign('chufa_date_str',$chufa_date_str);	

		
		
		$where_tuijian['recommend'] = 1;
		$where_tuijian['type'] = 2;
		$where_tuijian['shangjia'] = 1;
		$list = $m->field('id,pic_url')->where($where_tuijian)->limit(5)->select();
		$this->assign('list',$list);
		
		$where_rexiao['rexiao'] = 1;
		$where_rexiao['type'] = 2;
		$where_rexiao['shangjia'] = 1;
		$list_rexiao = $m->field('id,pic_url,price_new')->where($where_rexiao)->limit(2)->select();
		$this->assign('list_rexiao',$list_rexiao);
		
		$pic_tuji = unserialize($result['pic_tuji']);
		$this->assign('pic_tuji',$pic_tuji);		
				

		
		$info = M('Single');
		$list_1 = $info->field('id,single_title,type')->where('type="新手指南"')->select();
		$this->assign('list_1',$list_1);
		
		$list_2 = $info->field('id,single_title,type')->where('type="购物指南"')->select();
		$this->assign('list_2',$list_2);
		
		$list_3 = $info->field('id,single_title,type')->where('type="支付方式"')->select();
		$this->assign('list_3',$list_3);
		
		$list_4 = $info->field('id,single_title,type')->where('type="客服中心"')->select();
		$this->assign('list_4',$list_4);


		$member = M('member','vip_');
		$where_m['id'] = $_SESSION['user_id'];
		$res_member = $member->where($where_m)->find();
		$this->assign('res_member',$res_member);

		$this->display();
	}	
	//关于我们
	public function about_us(){
		$m = M('page');
		$result = $m->where("columu='about_us'")->find();
		$result['create_time'] = date('Y-m-d',$result['create_time']);
		$this->assign('result',$result);
		
		$info = M('Single');
		$list_1 = $info->field('id,single_title,type')->where('type="新手指南"')->select();
		$this->assign('list_1',$list_1);
		
		$list_2 = $info->field('id,single_title,type')->where('type="购物指南"')->select();
		$this->assign('list_2',$list_2);
		
		$list_3 = $info->field('id,single_title,type')->where('type="支付方式"')->select();
		$this->assign('list_3',$list_3);
		
		$list_4 = $info->field('id,single_title,type')->where('type="客服中心"')->select();
		$this->assign('list_4',$list_4);
		$this->display();
	}

	
	//添加足迹
	public function add_footprint(){
		$info = M('FootPrint');
		$_POST['uid'] = $_SESSION['user_id'];	
		$_POST['gid'] = I('gid');
		$_POST['goods_pic'] = I('goods_pic');
		$_POST['goods_name'] = I('goods_name');
		$_POST['goods_price'] = I('goods_price');
		$_POST['create_time'] = time();
		$result = $info->where(array('uid'=>$_POST[uid],'gid'=>$_POST[gid]))->select();
		if(!$result){ 
			$info->add($_POST);
		}else{
			$info->where(array('uid'=>$_POST[uid],'gid'=>$_POST[gid]))->save($_POST); 
		}
	}
	
	//搜索
	public function search(){
		$m = M('Goods');
		$where['title'] = array('like','%'.$_POST['title'].'%');
		$where['shangjia'] = array('eq',1);
		$nowPage = isset($_GET['p'])?$_GET['p']:1;
		$result = $m->where($where)->order('sort_num ASC')->page($nowPage.','.PAGE_SIZE)->select();
		
		//分页
    	$count = $m->where($where)->count(id);		// 查询满足要求的总记录数
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	
    	$this->assign('result',array('data' => $result, 'page' => $show));
		$this->display();
	}
	
	
	//单页详情
	public function single_details(){
		$id = $_GET['id'];
		$info = M('Single');
		$result = $info->field('single_title,single_info')->where(array('id'=>$id))->find();
		$this->assign('result',$result);
		$this->display();
	}


	//限购商品列表
	public function xiangou(){	
		$m = M('goods_class');
		$where3['fid'] = 0;
		$where3['type'] = 1;
		$where3['hide_status'] = 0;
		$result_class = $m->where($where3)->select();

		$this->assign('result_class',$result_class);
		
		$where_sub['fid'] = $_GET['class_id'];
		$where_sub['hide_status'] = 0;
		$class_sub = $m->where($where_sub)->select();
		//echo $m->getlastsql();
		$this->assign('class_sub',$class_sub);

    	$m = M('goods');
    	
		if($_GET['class_sub_id']){
			$where['class_id'] = $_GET['class_sub_id'];			
		}		
		if($_GET['class_id']){
			$where['class_fid'] = $_GET['class_id'];
			$this->assign('class_id',$_GET['class_id']);
		}
		if($_GET['price'] == 1){
			$where['price_new'] = array('elt',100);
		}
		if($_GET['price'] == 2){
			$where['price_new'] = array('between','100,500');
		}
		if($_GET['price'] == 3){
			$where['price_new'] = array('between','500,1000');
		}
		if($_GET['price'] == 4){
			$where['price_new'] = array('egt',1000);
		}
		
		if(!empty($_POST['title'])){
			$where['title'] = array('like',"%".$_POST['title']."%");
		}
		$where['shangjia'] = array('eq',1);
		$where['xiangou'] = array('eq',1);
		$nowPage = isset($_GET['p'])?$_GET['p']:1;
    	// page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
    	$result = $m->where($where)->order('sort_num ASC')->page($nowPage.','.PAGE_SIZE)->select();
		
    	foreach ($result as $k=>$v){
    		$result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
    	}

    	$this->assign('result',$result);
		$this->assign('post',$_POST);
    	
		$info = M('Single');
		$list_1 = $info->field('id,single_title,type')->where('type="新手指南"')->select();
		$this->assign('list_1',$list_1);
		
		$list_2 = $info->field('id,single_title,type')->where('type="购物指南"')->select();
		$this->assign('list_2',$list_2);
		
		$list_3 = $info->field('id,single_title,type')->where('type="支付方式"')->select();
		$this->assign('list_3',$list_3);
		
		$list_4 = $info->field('id,single_title,type')->where('type="客服中心"')->select();
		$this->assign('list_4',$list_4);
		
    	//分页
    	$count = $m->where($where)->count(id);		// 查询满足要求的总记录数
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	$this->assign('page',$show);// 赋值分页输出   	
    	$this->display();
	}

    //定义奖项概率
    private function sj_num()
    {
	    $arr=range(0,99);//定义一个概率的数组
		$new_arr=array_count_values($arr);//获取每个元素的个数
		$m=M('jiangpin');
		$this->results=$m->select();
		/*
		 * 默认奖品概率
		 * 一等奖  1%
		 * 二等奖  5%
		 * 三等奖  10%
		 * 四等奖  15%
		 * 参与奖  20%
		 */
		foreach($this->results as $key=>$val){
			//echo $val["jiangpin_gailv"];
			//echo $key."=>>>".$new_arr[$key];
			if($val["jiangpin_gailv"]==0){
				$arr[$key]="a".$key+1;
			}else if($val["jiangpin_gailv"]>$new_arr[$key]){
				$cha=$val["jiangpin_gailv"]-$new_arr[$key];//相差的概率
				if($key==0){
					for($i=11;$i<$cha+11;$i++){
						$arr[$i]=$key+1;
					}
				}else if($key==1){
					for($i=21;$i<$cha+21;$i++){
						$arr[$i]=$key+1;
					}
				}else if($key==2){
					for($i=41;$i<$cha+41;$i++){
						$arr[$i]=$key+1;
					}
				}else if($key==3){
					for($i=61;$i<$cha+61;$i++){
						$arr[$i]=$key+1;
					}
				}else if($key==4){
					for($i=81;$i<$cha+81;$i++){
						$arr[$i]=$key+1;
					}
				}
			}else if($val["jiangpin_gailv"]<$new_arr[$key]){
				$cha=$new_arr[$key]-$val["jiangpin_gailv"];
				if($key==0){
					for($i=11;$i<$cha+11;$i++){
						$arr[$i]="a".$key;
					}
				}else if($key==1){
					for($i=21;$i<$cha+21;$i++){
						$arr[$i]="a".$key;
					}
				}else if($key==2){
					for($i=41;$i<$cha+41;$i++){
						$arr[$i]="a".$key;
					}
				}else if($key==3){
					for($i=61;$i<$cha+61;$i++){
						$arr[$i]="a".$key;
					}
				}else if($key==4) {
					for ($i = 81; $i < $cha+81; $i++) {
						$arr[$i] = "a" . $key;
					}
				}
			}
		}

		$suiji=$arr[array_rand($arr)];
		/*echo $suiji;*/
		return $suiji;
		/*
		 * 判断概率是否为0 为0的话，在数组中删除对应的奖品值
		 * 判断是否小于或大于概率 若问哦大于概率则增加对应奖励的值，小于则删除
		 *
		 */
    }
	//抽奖页面  
	public function choujiang(){
		
		if(empty($_SESSION['user_id']) || empty($_SESSION['mobile'])){
				//要回调的url
				$this->redirect('Public/login');	//请先登录
				exit;
			}
		//自己的中奖记录
		$where_my['user_id'] = $_SESSION['user_id'];
		$result = M('choujiang')->where($where_my)->order('id DESC')->limit('30')->select();
		foreach($result as $k=>$v){
			$result[$k]['mobile'] = substr_replace($v['mobile'],'****',3,4);
			$result[$k]['choujiang_time'] = date('H:i:s',$v['choujiang_time']);
		}
		$this->assign('result',$result);
		//查询用户积分
		$user=M('user');
		$my=$user->field('integral,add_jf_currency,add_jf_limit')->where(array('id'=>$_SESSION['user_id']))->find();
		$jfa=$my['add_jf_limit'];
		$jfb=$my['add_jf_currency'];
		//dump($my);
		$this->assign("jfa",$jfa);
		$this->assign("jfb",$jfb);
		//全部人中奖记录
		$where_all['jiangpin_dengji'] = array('neq','未中奖');
		$result_all = M('choujiang')->where($where_all)->order('id DESC')->limit('30')->select();
		foreach($result_all as $k=>$v){
			$result_all[$k]['mobile'] = substr_replace($v['mobile'],'****',3,4);
			$result_all[$k]['choujiang_time'] = date('H:i:s',$v['choujiang_time']);
		}
		//获取每次抽奖扣取的积分
			$choujiang_xianzhi=M('choujiang_xianzhi');
			$low_jf=$choujiang_xianzhi->where(array('id'=>1))->getField('xianzhi_jifen');
			$this->assign('low_jf',$low_jf);
		$jiangpin=M('jiangpin');//获取奖品表中的信息
		$results=$jiangpin->select();
		$this->assign('result_all',$result_all);
		$this->assign('results',$results);
		//dump(session());
		$this->display();
	}

	//抽奖
	public function choujiang_add(){
		if(IS_AJAX){
			
			if(empty($_SESSION['user_id']) || empty($_SESSION['mobile'])){
				//要回调的url
				$this->ajaxReturn('no_login');	//请先登录
				exit;
			}
			
			$m=M('choujiang');
			$choujiang_xianzhi=M('choujiang_xianzhi');
			$where['id']=$this->sj_num();
			$start_time=date('Y-m-d 0:0:0',NOW_TIME);
			$end_time=date('Y-m-d 23:59:59',NOW_TIME);
			$start_time=strtotime($start_time);
			$end_time=strtotime($end_time);
			$times=$m->order('choujiang_time')->where(array('user_id'=>$_SESSION['user_id'],'choujiang_time'=>array(array('gt',$start_time),array('lt',$end_time))))->count();
			//file_put_contents('times.txt',$m->getlastsql());
			$set_times=$choujiang_xianzhi->where(array('id'=>1))->getField('times');
			if($set_times!=0 && $times>=$set_times){
				$this->ajaxReturn(888);//你今日的抽奖次数已用完,明天再来吧
				exit;
			}
			$jiangpin=M('jiangpin');//获取奖品表中的信息
			$rs=$jiangpin->where($where)->find();
			//获取每次抽奖扣取的积分
			
			$low_jf=$choujiang_xianzhi->where(array('id'=>1))->getField('xianzhi_jifen');
			
			//查询用户积分
			$user=M('user');
			$member=M('member','vip_');
			$my=$user->where(array('id'=>$_SESSION['user_id']))->find();		
			$grade=$member->where(array('id'=>$_SESSION['user_id']))->getField('grade_name');
			$jfa=$my['add_jf_limit'];
			$jfb=$my['add_jf_currency'];
			if($jfa+$jfb<$low_jf){
				$this->ajaxReturn(6);//积分不足
				exit;
			}
			if($grade=='会员'){
				if($jfa>$low_jf){
					$user->where(array('id'=>$_SESSION['user_id']))->setDec('add_jf_limit',$low_jf);
					$user->where(array('id'=>$_SESSION['user_id']))->setDec('integral',$low_jf);
				}elseif($jfa>0){
					$user->where(array('id'=>$_SESSION['user_id']))->setDec('add_jf_limit',$jfa);
					$user->where(array('id'=>$_SESSION['user_id']))->setDec('add_jf_currency',$low_jf-$jfa);
					$user->where(array('id'=>$_SESSION['user_id']))->setDec('integral',$low_jf);
				}elseif($jfb>$low_jf){
					$user->where(array('id'=>$_SESSION['user_id']))->setDec('add_jf_currency',$low_jf);
					$user->where(array('id'=>$_SESSION['user_id']))->setDec('integral',$low_jf);
				}else{
					$this->ajaxReturn(6);//积分不足
				}
			}elseif($grade=='合伙人'){
				if($jfb>$low_jf){
					$user->where(array('id'=>$_SESSION['user_id']))->setDec('add_jf_currency',$low_jf);
					$user->where(array('id'=>$_SESSION['user_id']))->setDec('integral',$low_jf);
				}else{
					$this->ajaxReturn(6);//积分不足
				}
			}else{
				$this->ajaxReturn(6);//积分不足
			}
			$add['user_id']=$_SESSION['user_id'];
			$add['choujiang_time']=time();
			$add['mobile']=$_SESSION['mobile'];
			if($rs){
				$add['jiangpin_dengji']=$rs["jiangpin_dengji"];
				$add['jiangpin_name']=$rs["jiangpin_name"];
				$add['is_win']=1;
			}else{
				$add['jiangpin_dengji']="未中奖";
				$add['jiangpin_name']="谢谢参与";
			}
			$add['use_jifen']=$low_jf;
			$add['status']=0;
			$add['ip']=get_client_ip();
			$result=$m->add($add);
			$this->ajaxReturn($where['id']);
			/* if(empty($result)){
				$this->ajaxReturn(0);
			}else{
				$this->ajaxReturn(1);
			} */
		}

	}

	//限时抢购
	public function xianshi(){
		$m=M('goods');
		$where['xianshi_status']=1;
		$this->result=$m->where($where)->select();
		$this->display();
	}
/**
	public function jf(){
		//计算返利积分
		//echo 1234;exit;
		$goods=M('goods','db_');
		$goods_orders=M('goods_orders','db_');
		$goods_orders_record=M('goods_orders_record','db_');
		
		$data=$goods->select();
		foreach($data as $key=>$val){
			$goods_orders_record->where(array('goods_id'=>$val['id']))->save(array('fanli_jifen'=>$val['fanli_jifen']));
		}
		
		exit;
		$fanli_jf=0;
		foreach($data as $kk=>$vv){
			$good_ids=$goods_orders_record->field('goods_id,goods_num')->where(array('goods_orders_id'=>$vv['id']))->select();
			foreach($good_ids as $k=>$v){
				$jf=$goods->where(array('id'=>$v['goods_id']))->getField('fanli_jifen');
				
				$jf_sum=$jf*$v['goods_num'];
				$fanli_jf+=$jf_sum;
				$jf=0;
			}
			$goods_orders->where(array('id'=>$vv['id']))->save(array('fanli_jifen'=>$fanli_jf));
			
			$fanli_jf=$fanli_jf=0;
		}
		
			
			
		
	}
	**/
	
	//团购
	public function tuangou(){	
		$m = M('goods_class');
		$where3['fid'] = 0;
		$where3['type'] = 1;
		$where3['hide_status'] = 0;
		$result_class = $m->where($where3)->select();

		$this->assign('result_class',$result_class);
		
		$where_sub['fid'] = $_GET['class_id'];
		$where_sub['hide_status'] = 0;
		$class_sub = $m->where($where_sub)->select();
		//echo $m->getlastsql();
		$this->assign('class_sub',$class_sub);

    	$m = M('goods');
    	
		if($_GET['class_sub_id']){
			$where['class_id'] = $_GET['class_sub_id'];			
		}		
		if($_GET['class_id']){
			$where['class_fid'] = $_GET['class_id'];
			$this->assign('class_id',$_GET['class_id']);
		}
		if($_GET['price'] == 1){
			$where['price_new'] = array('elt',100);
		}
		if($_GET['price'] == 2){
			$where['price_new'] = array('between','100,500');
		}
		if($_GET['price'] == 3){
			$where['price_new'] = array('between','500,1000');
		}
		if($_GET['price'] == 4){
			$where['price_new'] = array('egt',1000);
		}
		
		if(!empty($_POST['title'])){
			$where['title'] = array('like',"%".$_POST['title']."%");
		}
		$where['shangjia'] = array('eq',1);
		$where['is_tuangou'] = array('eq',1);
		$nowPage = isset($_GET['p'])?$_GET['p']:1;
    	// page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
    	$result = $m->where($where)->order('sort_num ASC')->page($nowPage.','.PAGE_SIZE)->select();

		//echo $m->getlastsql();
		
    	foreach ($result as $k=>$v){
    		$result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
    	}

    	$this->assign('result',$result);
		$this->assign('post',$_POST);
    	
		$info = M('Single');
		$list_1 = $info->field('id,single_title,type')->where('type="新手指南"')->select();
		$this->assign('list_1',$list_1);
		
		$list_2 = $info->field('id,single_title,type')->where('type="购物指南"')->select();
		$this->assign('list_2',$list_2);
		
		$list_3 = $info->field('id,single_title,type')->where('type="支付方式"')->select();
		$this->assign('list_3',$list_3);
		
		$list_4 = $info->field('id,single_title,type')->where('type="客服中心"')->select();
		$this->assign('list_4',$list_4);
		
    	//分页
    	$count = $m->where($where)->count(id);		// 查询满足要求的总记录数
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	$this->assign('page',$show);// 赋值分页输出   	
    	$this->display();
	}	
}





