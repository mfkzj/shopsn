<?php
namespace API\Controller;

use Common\Tool\Tool;
use API\Model\AdviseModel;

class LeaveingAMessageController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    
        $this->isLogin();
    }
    
    public function liuyan()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('mobile')), false, array('content', 'mobile', 'app_type')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
       
        //验证手机
        Tool::connect('ParttenTool');
        
        $status = Tool::validateData($_POST['mobile'], 'mobile');
        
        $this->prompt($status, null, '手机格式有误');
        
        $status = AdviseModel::getInitation()->add($_POST);
       
        $this->updateClient($status, '留言', true);
     
    }
        // 消息
    public function  news()
    {
        $user_id = $_REQUEST['id']; // 会员id
        $sql = "SELECT * FROM `db_news` WHERE `sendto` IN ('0','$user_id') ORDER BY create_time desc ";
        $query = @mysql_query($sql, $connect);
        while ($result = @mysql_fetch_assoc($query)) {
            $data[] = $result;
        }
        foreach ($data as $k => $v) {
            $data[$k]['create_time'] = date("Y-m-d H:i:s", $v['create_time']);
        }
    
        return Response::show('200', '消息', $data);
    }
}