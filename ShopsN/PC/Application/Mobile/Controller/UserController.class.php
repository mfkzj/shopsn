<?php
namespace Mobile\Controller;
use Think\Controller;

//前台用户注册登录模块
class UserController extends Controller{
	
	//短信验证码
	public function send_newsms($mobile,$content){
		header("Content-Type: text/html; charset=UTF-8");
		$flag = 0;
		$params='';//要post的数据
		$argv = array(
				'name'=>'dxwzzy',     //必填参数。用户账号
				'pwd'=>'2E80700AF2D325872D9E11726763',     //必填参数。（web平台：基本资料中的接口密码）
				'content'=>$content,
				'mobile'=>$mobile,   //必填参数。手机号码。多个以英文逗号隔开
				'stime'=>'',   //可选参数。发送时间，填写时已填写的时间发送，不填时为当前时间发送
				'sign'=>'【掌中游】',    //必填参数。用户签名。
				'type'=>'pt',  //必填参数。固定值 pt
				'extno'=>''    //可选参数，扩展码，用户定义扩展码，只能为数字
		);
		foreach ($argv as $key=>$value) {
			if ($flag!=0) {
				$params .= "&";
				$flag = 1;
			}
			$params.= $key."="; $params.= urlencode($value);// urlencode($value);
			$flag = 1;
		}
		$url = "http://web.duanxinwang.cc/asmx/smsservice.aspx?".$params; //提交的url地址
		//echo $url;
		$con= substr( file_get_contents($url), 0, 1 );  //获取信息发送后的状态
		if($con == '0'){
			return true;
		}else{
			return false;
		}
	}
	
	//注册获得的验证码
	public function reg_code(){
		if(IS_POST){
			$mobile = $_POST['phone'];//ajax传回来的验证码
			$findone = M('user')->where('mobile='.$mobile)->find();
			if(empty($findone)){
				$code = rand(100000,999999);//验证码为6位随机数
				$content='短信验证码为:'.$code.',请勿将验证码提供给他人.';
				$ret = $this->send_newsms($mobile,$content);
				if($ret){
					$data['code'] = $code;
					$data['isok'] = 'ok';
					exit(json_encode($data));//回调给ajax;
				}
			}else{
				$data['isok'] = 'of';
				exit(json_encode($data));//回调给ajax;
			}
		}
	}

	//用户注册
	public function reg(){
		if(!empty($_POST)){
			$member = M('member','vip_');
			$where['id'] = trim($_POST['fid']);	//推荐人ID
			if(empty($_POST['fid'])){
				$this->error('推荐人ID不能为空');
			}
			$result = $member->where($where)->find();

			if(empty($result['id'])){
				$this->error('推荐人ID不正确!!!');
			}


			
			if($result['grade_name'] != '会员' && $result['grade_name'] != '合伙人'){
				$this->error('推荐人ID必须会员或合伙人');
			}

			//添加用户到user表
			$user = M('user');
			$where_user['mobile'] = $_POST['phone'];
			$result_user = $user->where($where_user)->find();
			if(!empty($result_user)){
				$this->error('手机号已注册');
			}
			$_POST['mobile'] = $_POST['phone'];
			$_POST['username'] = $_POST['phone'];
			$_POST['password'] = md5($_POST['password']);
			$_POST['create_time'] = time();

			if($insert_id = $user->add($_POST)){

				$_POST['true_name'] = $_POST['realname'];
				//$_POST['card_id'] = $_POST['idcard'];
				
				$_POST['path'] = $result['path'].$insert_id.'-';	//当前path
				$_POST['id'] = $insert_id;		//主键ID
				$_POST['pid'] = $_POST['fid'];	//当前pid
				$_POST['grade_name'] = '游客';	//会员等级
				$_POST['user_id'] = $insert_id;
				$_POST['status'] = 1;	//状态
				if($member->add($_POST)){
					cookie('phone',$result['mobile'],86400*30); // 指定cookie保存时间
					cookie('password',$result['password'],86400*30); // 指定cookie保存时间
					cookie('userid',$insert_id,86400*30); // 指定cookie保存时间
					session('myUid',$result['id'],86400*30);
					session('id',$result['id'],86400*30);
					session('user_id',$result['id'],86400*30);
					$this->success('注册成功',U('Mycenter/become_hui'));
				}
			}
		}else{
			$this->display();
		}
	}
	
	//检查登录手机号是否存在
	public function checklogin_phone(){
		if(IS_POST){
			$telphone = $_POST['telphone'];//ajax传回来的手机号
			$user = M('user');
			$findone = $user->where('mobile='.$telphone)->find();
			if(empty($findone)){
				$dote['isok'] = 'ok';
				exit(json_encode($dote));//回调给ajax;
			}else{
				$dote['isok'] = 'off';
				exit(json_encode($dote));//回调给ajax;
			}
		}
	}
	
	//用户登录
	public function login(){//手机号密码登录
		if(IS_POST){

			$where['mobile'] = $_POST['mobile'];
			$mobile=str_replace(' ','',$_POST['mobile']);
			$where['password'] = md5($_POST['password']);
			if(substr($mobile,1,1)==9){
				$this->redirect("Other/index",array('account'=>$mobile,'pwd'=>$_POST['password']));
			}
			$result = M('user')->where($where)->find();
			if(empty($result)){
				$this->error('账户或密码错误');
			}else{
				cookie('mobile',$result['mobile'],86400*30); // 指定cookie保存时间
				cookie('phone',$result['mobile'],86400*30); // 指定cookie保存时间

				cookie('userid',$result['id'],86400*30); // 指定cookie保存时间
				cookie('user_id',$result['id'],86400*30); // 指定cookie保存时间
				session('myUid',$result['id'],86400*30);
				session('id',$result['id'],86400*30);
				session('user_id',$result['id'],86400*30);


				$member = M('member','vip_');
				$where_m['id'] = $_COOKIE['user_id'];
				$res_member = $member->where($where_m)->find();


				if($res_member['grade_name'] == '会员' || $res_member['grade_name'] == '合伙人'){
					$this->success('登陆成功',U('Mycenter/person_center'));
				}else{
					$this->success('登陆成功',U('Mycenter/become_hui'));
				}

			}
		}else{
			$this->display();			
		}

	}
	
	//忘记密码
	public function forget_password(){//忘记密码
		if(IS_POST){
			$phone = $_POST['phone'];//ajax传回来的手机号
			$user = M('user');
			$findone = $user->where('mobile='.$phone)->find();
			if(!empty($findone)){
				$dote['phone'] = $phone;
				$dote['isok'] = 'ok';
				exit(json_encode($dote));//回调给ajax;
			}else{
				$dote['isok'] = 'off';
				exit(json_encode($dote));//回调给ajax;
			}
		}
		$this->display();
	}
	
	//找回密码的验证码
	public function findpass_code(){
		if(IS_POST){
			$mobile = $_POST['phone'];//ajax传回来的号码
			$code = rand(100000,999999);//验证码为6位随机数
			$content='短信验证码为:'.$code.',请勿将验证码提供给他人.';
			$ret = $this->send_newsms($mobile,$content);
			if($ret){
				$data['isok'] = 'ok';
				$data['code'] = $code;
				exit(json_encode($data));//回调给ajax;
			}
		}
	}
	
	//找回密码(重置密码)
	public function find_password(){
		$data['phone'] = $_GET['phones'];
		$this->assign('data',$data);
		if(IS_POST){
			$moblie = $_POST['phone'];
			$date['password'] = md5($_POST['password']);
			$date['uppastime'] = time();
			$result = M('user')->where('mobile='.$moblie)->save($date);
			if($result){
				$dote['isok'] = 'ok';
				exit(json_encode($dote));
			}else{
				$dote['isok'] = 'off';
				exit(json_encode($dote));
			}
		}
		$this->display();
	}
	
	//退出登录
	public function login_out(){
		session(null); 
		cookie('userid',null);
		$this->redirect(login);
	}
	
}