<?php
namespace API\Controller;

use API\Model\AdModel;
use API\Model\GoodsClassModel;

class IndexController extends BaseController 
{
    public function index()
    {
        
        $data = AdModel::getInitnation()->getAd(array(
            'field' => array('pic_url as banner_url', 'ad_link'),
            'where' => array('isshow' => 1, 'ad_space'=> 16),
            'order' => 'sort_num ASC',
            'limit' => 5
        ));
        
        $offset = ($_POST['page'] - 1) * $_POST['page_size'];
//      //首页推荐的分类
        $goods = GoodsClassModel::getInition()->getChildrens(array(
            'field' => 'pic_url as class_url,id as class_id,class_name,sort_num',
            'where' => array('fid' => ' <> 0', 'hide_status' => 0, 'type' => 1),
            'order' => 'sort_num DESC',
            'limit' => $offset.' , '.$_GET['page_size'],
        ));
        $data['clas'] = $goods;
        
        $this->ajaxReturnData($data);
    }
}