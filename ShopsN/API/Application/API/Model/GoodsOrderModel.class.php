<?php
namespace API\Model;

use Think\Model;

/**
 * 订单模型 
 */
class GoodsOrderModel extends Model
{
    private static $obj ;
    
    public static function getInitation()
    {
        $class = __CLASS__;
        return self::$obj = !(self::$obj instanceof $class) ? new self() : self::$obj;
    }
    
    protected function _before_update( &$data, $options)
    {
        $data['update_time'] = time();
        return $data;
    }
     
    protected function _before_insert(&$data, $options)
    {
        $data['update_time'] = time();
        $data['create_time'] = time();
        $data['order_type']  = 0;
        $data['fanli_jifen'] = 0;
        return $data;
    }
    
    public function add($data='', $options=array(), $replace=false)
    {
        if (empty($data) || empty($options))
        {
            return false;
        }
        $data = $this->create($data);
        return parent::add($data, $options, $replace);
    }
}