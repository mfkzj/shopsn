﻿<?php
/**
* 	配置账号信息
*/
class WxPayConf_pub
{
	//=======【基本信息设置】=====================================
	//微信公众号身份的唯一标识。审核通过后，在微信发送的邮件中查看
	static $APPID = 'wx7540195e3f15a095';
	//受理商ID，身份标识
	static $MCHID = '1367111102';
	//商户支付密钥Key。审核通过后，在微信发送的邮件中查看
	static $KEY = 'zzuzzu88zzuzzu88zzuzzu88zzuzzu88';
	
	//=======【基本信息设置】=====================================
	//微信公众号身份的唯一标识。审核通过后，在微信发送的邮件中查看
	static $pay_account = 'wx7540195e3f15a095';
	//受理商ID，身份标识
	static $seller_id = '1367111102';
	//商户支付密钥Key。审核通过后，在微信发送的邮件中查看
	static $pay_key = 'zzuzzu88zzuzzu88zzuzzu88zzuzzu88';
	//JSAPI接口中获取openid，审核后在公众平台开启开发模式后可查看
	static $APPSECRET = '0721cacceec462175f614367ec604ac6 ';
	
	//=======【JSAPI路径设置】===================================
	//获取access_token过程中的跳转uri，通过跳转将code传入jsapi支付页面
	const  JS_API_CALL_URL = C('domain').'/index.php/Mobile/Wxpay/new_pay';
	
	//=======【证书路径设置】=====================================
	//证书路径,注意应该填写绝对路径
	const SSLCERT_PATH = C('domain').'/Core/Library/Vendor/Wxpay/WxPayPubHelper/cacert/apiclient_cert.pem';
	const SSLKEY_PATH =  C('domain').'/Core/Library/Vendor/Wxpay/WxPayPubHelper/cacert/apiclient_key.pem';
	
	//=======【异步通知url设置】===================================
	//异步通知url，商户根据实际开发过程设定
	const NOTIFY_URL =  C('domain').'/index.php/Mobile/Wxpay/notify';

	//=======【curl超时设置】===================================
	//本例程通过curl使用HTTP POST方法，此处可修改其超时时间，默认为30秒
	const CURL_TIMEOUT = 30;
	
	/**
	 * 设置 全部静态属性 
	 * @param array $static 数据库查询出的数组
	 * @param string $setKey 例外的建
	 * @return bool
	 */
	public static function setStatic(array $static, $setKey = 'pay_key')
	{
	    if (empty($static))
	    {
	        return false;
	    }
        
	    $obj = new ReflectionClass(__CLASS__);	    
        
        $staticPrep = $obj->getStaticProperties();
        
       
        foreach ($static as $key => $value)
        {
            if (array_key_exists($key, $staticPrep)) {
                
                switch ($key) {
                    
                    case $setKey:
                        $APPSECRET = unserialize($value);
                        if (!empty($APPSECRET))
                        {
                            self::$APPSECRET = $APPSECRET['APPSECRET'];
                            self::$pay_key   = $APPSECRET[$setKey];
                        }
                        break;
                   default: self::$$key = $value;break;
                }
            }
        }
	}
}
	
?>