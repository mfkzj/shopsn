/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : zhang618

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-10-31 18:19:34
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_config_children`
-- ----------------------------
DROP TABLE IF EXISTS `db_config_children`;
CREATE TABLE `db_config_children` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `config_class_id` int(11) DEFAULT '1' COMMENT '内容分类编号',
  `show_type` varchar(20) DEFAULT 'input' COMMENT '展示类型',
  `type_name` varchar(50) DEFAULT NULL COMMENT '对应的name值',
  `update_time` varchar(20) DEFAULT NULL,
  `insert_time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_config_children
-- ----------------------------
INSERT INTO `db_config_children` VALUES ('2', '2', 'input', 'account', '1477907306', null);
