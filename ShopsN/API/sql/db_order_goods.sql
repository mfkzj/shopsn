/*
Navicat MySQL Data Transfer

Source Server         : yisu
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : yisushop

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-11-15 15:19:54
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_order_goods`
-- ----------------------------
DROP TABLE IF EXISTS `db_order_goods`;
CREATE TABLE `db_order_goods` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` int(11) DEFAULT '0' COMMENT '商品id',
  `goods_id` int(11) DEFAULT '0' COMMENT '商品编号',
  `goods_num` int(11) DEFAULT NULL COMMENT '商品数量',
  `goods_price` float(11,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_order_goods
-- ----------------------------
INSERT INTO `db_order_goods` VALUES ('12', '8', '933', '1', '85.00');
INSERT INTO `db_order_goods` VALUES ('13', '8', '766', '1', '299.00');
INSERT INTO `db_order_goods` VALUES ('14', '9', '766', '1', '299.00');
INSERT INTO `db_order_goods` VALUES ('15', '9', '933', '1', '85.00');
INSERT INTO `db_order_goods` VALUES ('18', '11', '778', '4', '139.00');
INSERT INTO `db_order_goods` VALUES ('19', '11', '779', '5', '206.00');
INSERT INTO `db_order_goods` VALUES ('20', '12', '780', '5', '3200.00');
INSERT INTO `db_order_goods` VALUES ('21', '13', '781', '5', '19800.00');
INSERT INTO `db_order_goods` VALUES ('22', '14', '782', '5', '100.00');
INSERT INTO `db_order_goods` VALUES ('23', '15', '783', '5', '160.00');
INSERT INTO `db_order_goods` VALUES ('24', '16', '783', '5', '160.00');
INSERT INTO `db_order_goods` VALUES ('25', '17', '783', '5', '160.00');
INSERT INTO `db_order_goods` VALUES ('26', '18', '783', '5', '160.00');
INSERT INTO `db_order_goods` VALUES ('27', '19', '783', '5', '160.00');
INSERT INTO `db_order_goods` VALUES ('28', '20', '783', '5', '160.00');
INSERT INTO `db_order_goods` VALUES ('29', '21', '783', '5', '160.00');
INSERT INTO `db_order_goods` VALUES ('30', '22', '783', '5', '160.00');
INSERT INTO `db_order_goods` VALUES ('31', '23', '783', '5', '160.00');
INSERT INTO `db_order_goods` VALUES ('32', '24', '783', '5', '160.00');
INSERT INTO `db_order_goods` VALUES ('33', '25', '783', '5', '160.00');
INSERT INTO `db_order_goods` VALUES ('34', '26', '783', '5', '160.00');
