/*
Navicat MySQL Data Transfer

Source Server         : yisu
Source Server Version : 50617
Source Host           : localhost:3306
Source Database       : yisushop

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-11-15 15:20:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `db_order_comment`
-- ----------------------------
DROP TABLE IF EXISTS `db_order_comment`;
CREATE TABLE `db_order_comment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `goods_id` int(11) DEFAULT NULL COMMENT '商品编号',
  `goods_orders_id` int(11) DEFAULT NULL COMMENT '订单编号',
  `user_id` int(11) DEFAULT NULL COMMENT '用户编号',
  `status` smallint(6) DEFAULT '0' COMMENT '评价状态 0差评，1一般，2好评',
  `content` varchar(150) DEFAULT NULL,
  `create_time` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of db_order_comment
-- ----------------------------
INSERT INTO `db_order_comment` VALUES ('1', '933', '8', '10001037', '1', '很快将很快', null);
INSERT INTO `db_order_comment` VALUES ('2', '766', '8', '10001037', '1', '很快将很快', null);
