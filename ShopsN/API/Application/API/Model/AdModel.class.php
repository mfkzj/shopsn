<?php
namespace API\Model;
use Think\Model;
use Common\Tool\Tool;

/**
 * 广告模型 
 */
class AdModel extends Model
{
    private static $obj;
    
    public static function getInitnation()
    {
        $name = __CLASS__;
        return self::$obj = !(self::$obj instanceof $name) ? new self() : self::$obj;
    }
    /**
     * 获取 首页 banner图 
     */
    public function select($options = array(), Model $model)
    {
        if ( !($model instanceof Model) || !is_object($model) ) {
            return array();
        }
        $data = parent::select($options);
        
        if (!empty($data)) {
            foreach ($data as $key => &$value)
            {
                $value['product'] = $model->select(array(
                   'where' => array('shangjia' => 1, 'rexiao' => 1),
                   'limit' => 3,
                   'field' => array('id', 'pic_url','title')
                ));
            }
        }
        
        return $data;
    }
    /**
     * 获取 头部 中部 底部广告位 
     */
    public function getAd(array $options)
    {
        if (empty($options))
        {
            return $options;
        }
        //查找活动图片
        $data['banner'] = parent::select($options);

        //调取footer广告位
        $options['where']['ad_space_id'] = 33;
        $options['field'][0] = 'pic_url as hot_url';
        $options['limit'] = 4;
        $data['hot'] = parent::select($options);
        //调取center广告位
        $options['field'][0] = 'pic_url as center_url';
        $options['where']['ad_space_id'] = 20;
        $options['order'] = null;
        $data['center'] = parent::select($options);
        //转换类型
        return $data;
    }
} 