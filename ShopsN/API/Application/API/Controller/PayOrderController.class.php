﻿<?php
namespace API\Controller;

use Common\Tool\Tool;
use API\Model\OrderModel;
use Common\AlipayMobile\Lib\AlipayNotify;
use Common\UpacpApp\SDK\AcpService;
use Common\UpacpApp\SDK\LogUtil;
use Common\AlipayMobile\Lib\AopClient;
use Common\UpacpApp\SDK\SDKConfig;

/**
 * 支付控制器 
 */
class PayOrderController extends BaseController implements SDKConfig
{
    //微信
    // 更改商户把相关参数后可测试【后续添加到系统配置中，请耐心等待】
    const APP_ID = 'wxc60cf9d8efdbbd50';
    
    const APP_SECRET = "2e12d8c57e9a7066b92d3c83c83c1400"; // appsecret
    // 商户号，填写商户对应参数
    const MCH_ID = "1396980502";
    // 商户API密钥，填写相应参数
    const PARTNER_ID = "zzuzzu88zzuzzu88zzuzzu88zzuzzu88";
    const QQAPI      = 'https://api.mch.weixin.qq.com/pay/unifiedorder';
    
    //银联
    const MERID = "802310054110819";
    
    // 支付结果回调页面
    
    public function __construct()
    {
        parent::__construct();
    
        $this->isLogin();
    }
    
    //支付宝接口
    public function index()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('orders_num', 'price_shiji')), true , array('goods_title','orders_num','price_shiji')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
       
        $body = $_POST['goods_title'];//商品描述
        //在支付接口中返回要用的支付数据
        //$alipayNotify = new AopClient();
        self::validateOrder($_POST['orders_num']);
        //组装系统参数
        $data = array(
            "app_id"        => C('appId') ,//appid
            "version"		=> "1.0",
            "format"		=> "json",
            "method"		=> "alipay.trade.app.pay",
            "timestamp"		=> date("Y-m-d H:i:s",time()),
            "charset"		=>"utf-8",
            "sign_type"     => "RSA", //无需修改
            "notify_url"	=> C('domain')."/API/PayOrder/alipayNotify",//回调地址
            "biz_content"	=> json_encode(array(
                "subject" 		=> $_POST['orders_num'],//商品名称
                "out_trade_no"	=> $_POST['orders_num'],//商户网站唯一订单号
                "total_amount"	=> $_POST['price_shiji'],//总金额
                "seller_id"		=>"2088421281806684",//支付宝账号
                "product_code"	=>"QUICK_MSECURITY_PAY",
                "timeout_express" =>"60m",
            )),
        );
        try {
            $privateKey = file_get_contents(C('rsa'));
        } catch (\Exception $e) {
            $this->ajaxReturnData(null, '400', '未知错误');
        }
        Tool::connect('Token');
        $data = Tool::alipayToken($data, $privateKey);
        $code    = empty($data) ? '400' : '200';
        $message = empty($data) ? '支付失败': '订单支付中';
        echo json_encode(array(
            'message' => $message,
            'code'    => $code,
            'data'    => $data
        ));die();
    }
    
    /**
     * 支付通知 
     */
    public function alipayNotify()
    {
        $alipayNotify = new AopClient();
       
        $verify_result = $alipayNotify->rsaCheckV1($_POST,PRV);
        //file_put_contents("./10.txt",$verify_result);
        if($verify_result) {//验证成功
            //获取支付宝的通知返回参数，可参考技术文档中服务器异步通知参数列表
            //商户订单号
            $out_trade_no = $_POST['out_trade_no'];
            //支付宝交易号
            $trade_no = $_POST['trade_no'];
            //交易状态
            $trade_status = $_POST['trade_status'];
            if($_POST['trade_status'] == 'TRADE_FINISHED') {
        
            } else if ($_POST['trade_status'] == 'TRADE_SUCCESS') {
                //判断该笔订单是否在商户网站中已经做过处理
                //如果没有做过处理，根据订单号（out_trade_no）在商户网站的订单系统中查到该笔订单的详细，并执行商户的业务程序
                //请务必判断请求时的total_fee、seller_id与通知时获取的total_fee、seller_id为一致的
                //如果有做过处理，不执行商户的业务程序
                //注意：
                //退款日期超过可退款期限后（如三个月可退款），支付宝系统发送该交易状态通知
                //调试用，写文本函数记录程序运行情况是否正常
                //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
                $orders_num =$out_trade_no;
                $info_a = strpos($orders_num,'u');
                if($info_a==1){
                    //提成处理
                }else{
                    //修改商品
                    //修改数据库中的订单状态.1已支付
                    OrderModel::getInitation()->save(array(
                        'order_status' => 1
                    ),array(
                        'where' => array('user_id' => $_SESSION['userId'], 'order_sn_id' => $orders_num )
                    ));
                }
            }
            echo "success";
        }else{
            //验证失败
            echo "fail";
            //调试用，写文本函数记录程序运行情况是否正常
            //logResult("这里写入想要调试的代码变量值，或其他运行的结果记录");
        }
    }
}