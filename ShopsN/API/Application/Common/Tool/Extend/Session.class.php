<?php
namespace Common\Tool\Extend;

use Common\Tool\Tool;

class Session extends Tool
{
    public function setSession($isURL = '*')
    {
        ini_set('session.gc_maxlifetime', 3600);
        header("Access-Control-Allow-Origin:".$isURL);
        $sessionId = isset($_POST['sessionId']) ? $_POST['sessionId'] : null;
        if($sessionId){
            session_write_close();
            session_id($sessionId);
            session_start();
        }
    
    }
}