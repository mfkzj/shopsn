<?php
namespace Home\Model;
use Think\Model;

/**
 * 广告模型 
 */
class AdModel extends Model
{
    /**
     * 获取 首页 banner图 
     */
    public function select($options = array(), Model $model)
    {
        if ( !($model instanceof Model) || !is_object($model) ) {
            return array();
        }
        $data = parent::select($options);
        
        if (!empty($data)) {
            foreach ($data as $key => &$value)
            {
                $value['product'] = $model->select(array(
                   'where' => array('shangjia' => 1, 'rexiao' => 1),
                   'limit' => 3,
                   'field' => array('id', 'pic_url','title')
                ));
            }
        }
        
        return $data;
    }
} 