<?php
namespace Mobile\Controller;

use Mobile\Model\GoodsClassModel;
use Mobile\Model\AdSpaceModel;
use Common\Tool\Tool;

//前台首页模块
class IndexController extends BaseController
{
	
	//商品首页数据调取
	public function index() 
	{
		//判断用户是否登录
		if(!empty($_COOKIE['userid'])){
			$findone = M('user')->where('id='.$_COOKIE['userid'])->find();
			$this->assign('findone',$findone);
		}
		$bbb = M('user')->where('mobile = 15961728214')->select();
        
		//调取banner 
		$where_ad['isshow'] = 1;
		$where_ad['ad_space_id'] = 16;
		$banner = M('ad')->where($where_ad)->order('sort_num')->select();//banner活动图片
		$this->assign('banner',$banner);

		//首页 分类商品数据
		if (!S('product'))
		{
		    $data = GoodsClassModel::getInition()->select(array(
		        'where' => array('fid' => 0, 'hide_status' => 0, 'shoutui' => 1, 'type' => 1),
		        'field' => array('id', 'class_name', 'fid', 'description'),
		        'limit' => 6
		    ), new \Think\Model('goods'));
		    S('product', $data, 10);
		}
		$this->assist();
		$this->product = S('product');
		$this->display();
	}
    
	public function findClass()
	{
	    Tool::checkPost($_GET, array('is_numeric' => array('class_id')), true, array('class_id')) ? true : $this->error('灌水机制已打开', U('Index/index'));
	    

	    $data = GoodsClassModel::getInition()->select(array(
	        'where' => array('fid' => $_GET['class_id'], 'hide_status' => 0, 'type' => 1),
	        'field' => array('id', 'class_name', 'fid', 'description'),
	        'limit' => 6
	    ), new \Think\Model('goods'), 'class_id');
	    $this->assist();
	    $this->product = $data;
	    $this->display('index');
	}
	
	/**
	 * 辅助 死函数
	 */
	private function assist()
	{
	    $m = M('ad');
	    $result_ad3 = $m->where('ad_space_id=20')->limit(1)->select();
	    $this->assign('ad_2',$result_ad3);
	    $result_ad4 = $m->where('ad_space_id=33')->limit(1)->select();
	    $this->assign('ad_4',$result_ad4);
	    $result_ad_home = $m->where('id in (71,72,73,74,75)')->limit(1)->select();
	    $this->assign('ad_6',$result_ad_home);
	}
	
	public function getGoods()
	{
	    \Common\Tool\Tool::checkPost($_GET, array('is_numeric' => array('class_id')), true) ? true :$this->error('灌水机制已打开', U('Index/index'));
	
	    $data = GoodsClassModel::getInition()->getChildrens(array(
	        'where' => array('fid'=> $_GET['class_id'], 'hide_status' => 0),
	        'field' => array('id', 'class_name', 'pic_url')
	    ));
	    
	    if (empty($data))
	    {
	        $this->redirect('Goods/goods_goodslist', array('class_id' => $_GET['class_id']));
	    }
	    $this->class_page = $data;
	    $this->display('index');
	
	}
	//旅游首页数据调取
	public function tourism_index(){
		//判断用户是否登录
		if(!empty($_COOKIE['userid'])){
			$findone = M('user')->where('id='.$_COOKIE['userid'])->find();
			$this->assign('findone',$findone);
		}
		//调取banner
		$where_ad['isshow'] = 1;
		$where_ad['ad_space_id'] = 17;
		$banner = M('ad')->where($where_ad)->order('id desc')->select();//banner活动图片
		$this->assign('banner',$banner);
		//调取banner下的第一行商品
		$where_home_tuijian['home_tuijian'] = 1;
		$where_home_tuijian['type'] = 2;
		$where_home_tuijian['shangjia'] = 1;
		$result_home_tuijian = M('goods')->where($where_home_tuijian)->limit(4)->order('sort_num ASC')->select();
		$this->assign('result_home_tuijian',$result_home_tuijian);

		//调取center广告位
		$where_adcenter['isshow'] = 1;
		$where_adcenter['ad_space_id'] = 22;
		$center = M('ad')->where($where_adcenter)->limit("1")->find();
		$this->assign('center',$center);

		//查询手机端首页广告位
		$where_home_ad['ad_space_id'] = 34;
		$mobile_home_ad = M('ad')->where($where_home_ad)->order('sort_num')->limit("4")->select();
		//file_put_contents('xx.txt',M('ad')->getLastSql());
		$this->assign('mobile_home_ad',$mobile_home_ad);


		//调取footer广告位
		$where_adfooter['isshow'] = 1;
		$where_adfooter['ad_space_id'] = 23;
		$footer = M('ad')->where($where_adfooter)->limit("1")->find();
		$this->assign('footer',$footer);

		//首页推荐的分类
		$where_page['fid'] = 0;
		$where_page['type'] = 2 ;
		$where_page['hide_status'] = 0;
		$class_page = M('goods_class')->where($where_page)->order('sort_num')->limit(5)->select();

		$this->assign('class_page',$class_page);

		$where_page_sub['fid'] = array('neq',0);
		$where_page_sub['type'] = 2;
		$where_page_sub['hide_status'] = 0;		
		$class_page_sub = M('goods_class')->where($where_page_sub)->order('sort_num')->select();

		$this->assign('class_page_sub',$class_page_sub);		

		$this->display();
	} 
	
	/*
	 * 分类直接进入商品列表
	 * */
	public function goods_come(){

		$goods = M('Goods');
		$map['class_fid'] = I('get.sign');
		$map['type'] = 	I('get.type');


		$map['shangjia'] = 	1;

		if(!empty($_GET['class_id'])){
			$map['class_id'] = I('get.class_id');
		}

		$count = $list = $goods->where($map)->count();

		$list = $goods->where($map)->select();

		
		$this->assign('type',$map['type']);
		$this->assign('class_fid',$map['class_fid']);
		$this->assign('count',$count);
		$this->assign('list',$list);

		if(empty($_GET['lz'])){
			$sign['a'] = 0;
		}else{
			$sign['a'] = 1;
		}
		$this->assign('sign',$sign);




//查询分类
		$m = M('goods_class');
		$where['fid'] = $_GET['sign'];
		if(!empty($_GET['class_fid'])){
			$where['fid'] = $_GET['class_fid'];
		}
		$result_class = $m->where($where)->select();
		$this->assign('result_class',$result_class);
		//dump($result_class);

		$this->display();
	}


	public function lvyou_come(){

		$goods = M('Goods');
		$map['class_fid'] = I('get.sign');
		$map['type'] = 	2;
		$map['shangjia'] = 	1;

		if(!empty($_GET['class_id'])){
			$map['class_id'] = I('get.class_id');
		}

		$count = $list = $goods->where($map)->count();

		$list = $goods->where($map)->select();
		
		$this->assign('type',$map['type']);
		$this->assign('class_fid',$map['class_fid']);
		$this->assign('count',$count);
		$this->assign('list',$list);

		if(empty($_GET['lz'])){
			$sign['a'] = 0;
		}else{
			$sign['a'] = 1;
		}
		$this->assign('sign',$sign);




//查询分类
		$m = M('goods_class');
		$where['fid'] = $_GET['sign'];
		if(!empty($_GET['class_fid'])){
			$where['fid'] = $_GET['class_fid'];
		}
		$result_class = $m->where($where)->select();
		$this->assign('result_class',$result_class);
		//dump($result_class);

		$this->display();
	}
	
	
	/*
	 * ajax传值
	 */
	public function ajax_goods(){
		$goods = M('Goods');

		$map['shangjia'] = 	1;

		if(!empty($_POST['id'])){
			$map['class_id'] = I('post.id');
		}

		$count = $list = $goods->where($map)->count();

		$list = $goods->where($map)->select();
		if($count == 0){
			$this->ajaxReturn(0);
		}else{
			$this->ajaxReturn($list);
		}
		$this->display();
	}

	
	
	//会员首页数据调取
	public function member_index(){
		//判断用户是否登录
		if(!empty($_COOKIE['userid'])){
			$findone = M('user')->where('id='.$_COOKIE['userid'])->find();
			$this->assign('findone',$findone);
		}
		//调取banner
		$where_ad['isshow'] = 1;
		$where_ad['ad_space_id'] = 18;
		$banner = M('ad')->where($where_ad)->order('id desc')->select();//banner活动图片
		$this->assign('banner',$banner);
		//调取center广告位
		$where_adcenter['isshow'] = 1;
		$where_adcenter['ad_space_id'] = 24;
		$center = M('ad')->where($where_adcenter)->limit("1")->find();
		$this->assign('center',$center);
		//调取footer广告位
		$where_adfooter['isshow'] = 1;
		$where_adfooter['ad_space_id'] = 25;
		$footer = M('ad')->where($where_adfooter)->limit("1")->find();
		$this->assign('footer',$footer);

		if(empty($_COOKIE['ly'])){
			$sign['a'] = 0;
		}else{
			$sign['a'] = 1;
		}
		$this->assign('sign',$sign);

		$this->display();
	}
	
	//合伙人首页数据调取
	public function partner_index(){
		//判断用户是否登录
		if(!empty($_COOKIE['userid'])){
			$findone = M('user')->where('id='.$_COOKIE['userid'])->find();
			$this->assign('findone',$findone);
		}
		//调取banner
		$where_ad['isshow'] = 1;
		$where_ad['ad_space_id'] = 19;
		$banner = M('ad')->where($where_ad)->order('id desc')->select();//banner活动图片
		$this->assign('banner',$banner);
		//调取center广告位
		$where_adcenter['isshow'] = 1;
		$where_adcenter['ad_space_id'] = 26;
		$center = M('ad')->where($where_adcenter)->limit("1")->find();
		$this->assign('center',$center);
		//调取footer广告位
		$where_adfooter['isshow'] = 1;
		$where_adfooter['ad_space_id'] = 27;
		$footer = M('ad')->where($where_adfooter)->limit("1")->find();
		$this->assign('footer',$footer);

		if(empty($_COOKIE['ly'])){
			$sign['a'] = 0;
		}else{
			$sign['a'] = 1;
		}
		$this->assign('sign',$sign);

		$this->display();
	}
	
	
	/*
	 * 搜索
	*/
	public  function goods_search(){
		$title = $_POST['title'];//商品标题
		$goods = M('Goods');
		$map = array();
		$map['title'] = array('like',"%$title%");
		$map['type'] = 1;
		$map['shangjia'] = 1;
		$count = $list = $goods->where($map)->count();

		$list = $goods->where($map)->select();
		$this->assign('count',$count);
		$this->assign('list',$list);

		$this->display();
	}

	public  function lvyou_search(){
		$title = $_POST['title'];//商品标题
		$goods = M('Goods');
		$map = array();
		$map['title'] = array('like',"%$title%");
		$map['type'] = array('eq',2);
		$map['shangjia'] = array('eq',1);
		$count = $list = $goods->where($map)->count();

		$list = $goods->where($map)->select();
		//echo $goods->getlastsql();
		$this->assign('count',$count);
		$this->assign('list',$list);

		if(empty($_COOKIE['ly'])){
			$sign['a'] = 0;
		}else{
			$sign['a'] = 1;
		}
		$this->assign('sign',$sign);

		$this->display();
	}	

	//收藏
	public function shoucang(){
		if(empty($_COOKIE['user_id'])){
			$this->redirect('User/login');
		}
		$m = M('goods_shoucang');
		$where['user_id'] = $_COOKIE['user_id'];
		$where['goods_id'] = $_GET['goods_id'];
		$result = $m->where($where)->find();
		if(!empty($result)){
			$this->error('已收藏');
		}

		$data['user_id'] = $_COOKIE['user_id'];
		$data['goods_id'] = $_GET['goods_id'];
		$data['create_time'] = time();
		$data['pic_url'] = $_GET['pic_url'];
		$data['price_old'] = $_GET['price_old'];
		$data['detail_title'] = $_GET['detail_title'];
		$data['price_new'] = $_GET['price_new'];
		$data['is_type'] = 2;

		if($m->add($data)){
			$this->success('收藏成功');
		}else{
			$this->error('收藏失败');
		}
	}
}
