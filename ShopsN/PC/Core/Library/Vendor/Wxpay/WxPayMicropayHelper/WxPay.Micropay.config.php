<?php
/**
* 	配置账号信息
*/

class WxPayConf_micropay
{
	//=======【基本信息设置】=====================================
	//微信公众号身份的唯一标识。审核通过后，在微信发送的邮件中查看
	const APPID = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
	//受理商ID，身份标识
	const MCHID = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
	//商户支付密钥Key。审核通过后，在微信发送的邮件中查看
	const KEY = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
	
	
	//=======【基本信息设置】=====================================
	//微信公众号身份的唯一标识。审核通过后，在微信发送的邮件中查看
	static $pay_account = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
	//受理商ID，身份标识
	static $seller_id = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
	//商户支付密钥Key。审核通过后，在微信发送的邮件中查看
	static $pay_key = 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx';
	
	//=======【证书路径设置】=====================================
	//证书路径,注意应该填写绝对路径
	const SSLCERT_PATH = '';
	const SSLKEY_PATH = '';
	
	//=======【curl超时设置】===================================
	//本例程通过curl使用HTTP POST方法，此处可修改其超时时间，默认为30秒
	const CURL_TIMEOUT = 30;
	/**
	 * 设置 全部静态属性
	 * @param array $static 数据库查询出的数组
	 * @param string $setKey 例外的建
	 * @return bool
	 */
	public static function setStatic(array $static)
	{
	    if (empty($static))
	    {
	        return false;
	    }
	
	    $obj = new ReflectionClass(__CLASS__);
	
	    $staticPrep = $obj->getStaticProperties();
	
	    foreach ($static as $key => $value)
	    {
	        if (array_key_exists($key, $staticPrep)) 
	        {
                self::$$key = $value;
	        }
	    }
	}
}
	
?>