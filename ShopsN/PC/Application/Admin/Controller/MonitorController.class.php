<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Admin\Controller;
use Common\Controller\AuthController;
use Think\Auth;

class MonitorController extends AuthController{
	//监控vip到期时间
     public function jk_vip(){
		 //把过期的会员全部变成游客
		$member=M('member','vip_');
		$data=$member->field('id,grade_name,vip_end')->where(array('grade_name'=>'会员','vip_end'=>array('lt',NOW_TIME)))->select();
		foreach($data as $v){
			$member->where(array('id'=>$v['id']))->save(array('grade_name'=>'游客'));
		}
		echo 1;
	 }
	 //监控订单
     public function jk_order(){
		$goods_orders = M('goods_orders');
		$flag=1;
		$goods_orders->startTrans();
		$data=$goods_orders->field('id,orders_num')->where(array('is_used'=>0,'orders_status'=>array('in',"1,2,3"),'fahuo_time'=>array('elt',NOW_TIME-15*86400)))->select();
		if(!$count($data)){
			$this->error("无处理事项!");
			exit;
		}
		foreach($data as $v){
			if($this->cl_order($v['id'])){
				$flag=0;
			}
		}
		if($flag){
			$goods_orders->commit();
		}else{
			$goods_orders->rollback();
		}
		
		$this->success("处理完成!");
		//$this->error("该功能等待测试!");
	 }
	 //处理订单
     private function cl_order($orders_id){
		$goods_orders = M('goods_orders');
		$is_use=$goods_orders->where(array('id'=>$orders_id,'is_used'=>1))->count();
		if(!$is_use){
			$map['orders_status']=5;
			$map['fanli_action']=session("account").'触发，时间：'.date("Y-m-d H:i:s",NOW_TIME);
			$map['shouhuo_time']=NOW_TIME;
			$rst=$goods_orders->where(array('id'=>$orders_id))->save($map);
			
			if($rst){
				R("Home/Shop/buy",array($orders_id));
			}
		}
	 }
}