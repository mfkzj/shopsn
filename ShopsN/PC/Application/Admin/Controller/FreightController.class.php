<?php
/**
 * Created by PhpStorm.
 * User: x
 * Date: 2016/8/12
 * Time: 14:52
 */

namespace Admin\Controller;


use Think\Controller;

class FreightController extends Controller
{

    //展示首页
    public function admin_index(){
        $model=M('Freight');
        $modelss=M('region');
        $rows=$model->select();
        foreach($rows as &$row){
            //获取省的名字
           $province=$modelss->where(array('areaid'=>$row['province']))->find();
            //获取城市的名字
           $city=$modelss->where(array('areaid'=>$row['city']))->find();
            //获取区的名字
           $area=$modelss->where(array('areaid'=>$row['area']))->find();
            $row['name']=$province['name'].$city['name'].$area['name'];
        }
        $this->assign('rows',$rows);
        $this->display('freight_list');
    }

    //展示编辑页面
   public function admin_edit(){
     $model=M('region');
       $rows=$model->where(array('parentid'=>0))->select();
	//dump($rows);
       $this->assign('rows',$rows);
    $this->display('freight_edit');
   }


    //获取城市
    public function province(){
        $id=I('post.id');
        //dump($id);
        //exit;
        $model=M('region');
        $rows=$model->where(array('parentid'=>$id))->select();
//dump($rows);
        //exit;
        $this->ajaxReturn($rows);
    }

    //>>添加数据
    public function add(){
        $data=I('post.');
		$addAll = array();
		foreach($data['province'] as $k => $item){
			$addAll[] = array(
				'province'=>$item,
				'ykg'=>$data['ykg'],
				'money'=>$data['money'],
				'onemoney'=>$data['onemoney'],
				'id'=>$data['id'],
			);
		}
        $model=M('Freight');
        $model->addAll($addAll);
        $this->success('添加成功!',U('Freight/admin_index'));
    }

    public function changeStatus(){
        $id=I('get.id');
        $model=M('Freight');
        $model->where(array('id'=>$id))->delete();
        $this->success('删除成功!',U('Freight/admin_index'));
    }
	
}