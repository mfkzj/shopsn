<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Admin\Controller;
use Common\Controller\AuthController;
use Think\Auth;

//活动管理
class ArticleController extends AuthController {
	
	//活动列表
    public function article_list(){
    	$m = M('article');
    	$nowPage = isset($_GET['p'])?$_GET['p']:1;
    	if(!empty($_POST['title'])){
    		$where['title'] = array('like','%'.$_POST['title'].'%');
    	}
     	if(!empty($_POST['keyword'])){
    		$where['keyword'] = array('like','%'.$_POST['keyword'].'%');
    	}
    	// page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
    	$result = $m->where($where)->order('sort_num ASC')->page($nowPage.','.PAGE_SIZE)->select();
    	$nid = count($result);
    	foreach ($result as $k=>$v){
    		$result[$k]['create_time'] = date('Y-m-d',$v['create_time']);
    		$result[$k]['nid'] = $nid--;
    	}
    	//分页
    	$count = $m->where($where)->count(id);		// 查询满足要求的总记录数
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	$this->assign('page',$show);// 赋值分页输出
    	$this->assign('result',$result);
    	$this->display();
    }
    
    //添加活动
    public function article_add(){
    	if(!empty($_POST)){
    		$upload = new \Think\Upload();// 实例化上传类
    		$upload->maxSize = 3145728;// 设置附件上传大小
    		$upload->exts = array('jpg', 'gif', 'bmp', 'png', 'jpeg');// 设置附件上传类型
    		$upload->rootPath = './Public/Uploads/article/';		//设置文件根目录
    		//上传文件
    		$info = $upload->upload();
    		$_POST['pic_url'] = $info['pic_url']['savepath'].$info['pic_url']['savename'];	//上传文件的路径    		
			$m = M('article');
			$_POST['create_time'] = time();
			if($m->add($_POST)){
				$this->success('添加成功');
			}else{
				$this->success('添加失败');
			} 		
    	}else{
    		$this->display();
    	}    	
    }
    
    //编辑
    public function article_edit(){
    	if(!empty($_POST)){
    		$upload = new \Think\Upload();// 实例化上传类
    		$upload->maxSize = 3145728;// 设置附件上传大小
    		$upload->exts = array('jpg', 'gif', 'bmp', 'png', 'jpeg');// 设置附件上传类型
    		$upload->rootPath = './Public/Uploads/article/';		//设置文件根目录
    		//上传文件
    		$info = $upload->upload();
    	    $_POST['pic_url'] = $info['pic_url']['savepath'].$info['pic_url']['savename'];	//上传文件的路径    			    			
    		if(empty($_POST['pic_url'])){
    			unset($_POST['pic_url']);
    		}
    		$where['id'] = $_POST['id'];	//活动ID
    		$m = M('article');
    		$_POST['update_time'] = time();		//更新时间
    		$result = $m->where($where)->save($_POST);
    		if($result){
    			$this->success('修改成功');
    		}else{
    			$this->error('修改失败');
    		}
    	}else{
    		$where['id'] = $_GET['id'];	//活动ID
    		$m = M('article');
    		$result = $m->where($where)->find();
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
    
    //删除
    public function article_del(){
    	$where['id'] = $_POST['id'];	//活动ID
    	$m = M('article');
    	$result = $m->where($where)->delete();
    	if($result){
    		$data['code'] = '1';	//删除成功
    		$this->ajaxReturn($data);
    	}else{
    		$data['code'] = '0';	//删除失败
    		$this->ajaxReturn($data);
    	}
    }
    
    //广告列表
    public function ad_list(){
    	$m = M('ad');
    	$result = $m->where($where)->order('id DESC')->page($_GET['p'].','.PAGE_SIZE)->select();
    	foreach ($result as $k=>$v){
    		$result[$k]['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
    	}
    	$this->assign('result',$result);
    	//分页
    	$count = $m->where($where)->count(id);		// 查询满足要求的总记录数
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	$this->assign('page',$show);// 赋值分页输出
    	$this->display();
    }
    
    //添加广告
    public function ad_add(){
    	if(!empty($_POST)){
    	$upload = new \Think\Upload();// 实例化上传类
    		$upload->maxSize = 3145728;// 设置附件上传大小
    		$upload->exts = array('jpg', 'gif', 'bmp', 'png', 'jpeg');// 设置附件上传类型
    		$upload->rootPath = './Public/Uploads/ad/';		//设置文件根目录
    		//上传文件
    		$info = $upload->upload();
    		$_POST['pic_url'] = $info['pic_url']['savepath'].$info['pic_url']['savename'];	//上传文件的路径    		
    		if(!$info) {		// 上传错误提示错误信息
    			$this->error($upload->getError());
    		}else{		// 上传成功
    			$m = M('ad');
    			$_POST['create_time'] = time();
    			if($m->add($_POST)){
    				$this->success('添加成功');
    			}else{
    				$this->success('添加失败');
    			}
    		}
    	}else {
    		$this->display();
    	}
    }
    
    //删除广告
    public function ad_del(){
    	$m = M('ad');
    	$where['id'] = $_POST['id'];
    	$result = $m->where($where)->delete();
    	if(!empty($result)){
    		$data['code'] = 1;
    		$data['message'] = '修改成功';
    		$this->ajaxReturn($data);
    	}else{
    		$data['code'] = 0;
    		$data['message'] = '修改失败';
    		$this->ajaxReturn($data);
    	}
    }
    
    //更新排序
    public function article_sort(){
    	$m = M('article');
    	$str_id = explode(',', substr($_GET['str_id'],1));
    	$str_sort = explode(',', substr($_GET['str_sort'],1));
    	foreach ($str_id as $k=>$v){
    		$data['sort_num'] = $str_sort[$k];
    		$m->where('id='.$v)->save($data);
    	}
    	$this->ajaxReturn(1);
    }
	
	 //单页列表
    public function single_list(){
    	$info = M('single');
    	$count = $info->count();
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	$nowPage = isset($_GET['p'])?$_GET['p']:1;
    	$list = $info->page($_GET['p'].','.PAGE_SIZE)->order('type')->select();
    	$this->assign('list',$list);
    	$this->assign('page',$show);// 赋值分页输出
    	$this->display();
    }
    
    
    //单页详情编辑
    public function single_edit(){
    	if(IS_POST){
    		$info = M('Single');
    		$data['id'] = $_POST['id'];
    		$data['single_info'] = $_POST['single_info'];
    		$data['create_time'] = time();
    		$result = $info->save($data);
    		if(!$result){
    			$this->error('修改失败');
    		}else{
    			$this->success('修改成功');
    		}
    	}else{
    		$where['id'] = $_GET['id'];	//活动ID
    		$info = M('Single');
    		$result = $info->where($where)->find();
    		$this->assign('result',$result);
    		$this->display();
    	}
    }
    
    //单页详情
    public function single_details(){
    	$id = $_GET['id'];
    	$info = M('Single');
    	$result = $info->where(array('id'=>$id))->find();
    	$this->assign('result',$result);
    	$this->display();
    }
	
	  //消息列表
    public function news_list(){
    	$info = M('News');
    	$count = $info->count();
    	$page = new \Think\Page($count,PAGE_SIZE);		// 实例化分页类 传入总记录数和每页显示的记录数
    	$show = $page->show();		// 分页显示输出
    	$nowPage = isset($_GET['p'])?$_GET['p']:1;
    	$list = $info->page($_GET['p'].','.PAGE_SIZE)->order('create_time desc')->select();
    	$this->assign('list',$list);
    	$this->assign('page',$show);// 赋值分页输出
    	$this->display();
    }
    
	//添加消息
	public function news_add(){
		if(empty($_POST)){
			$this->display();
		}else{
			$news = M('news');
			$_POST['news_info'] = I('news_info'); 
			$_POST['create_time'] = time();
			if(!$news->create()){
				$this->error('数据过滤失败！');
			}else{
				$result = $news->add();
				if(!$result){
					$this->error(0);
				}else{
					$this->ajaxReturn(1);
				}
			}
		}
	}
	
	
	//删除消息
	public function news_del(){
		$news = M('news');
		$data['id'] = I('id');
		$result = $news->where($data)->delete();
		if(!$result){
			$this->ajaxReturn(0);
		}else{
			$this->ajaxReturn(1);
		}
	}
	
	
	//修改消息
	public function news_update(){
		$news = M('news');
		if(empty($_POST)){
			$data['id'] = I('id');
			$result = $news->field('news_info,id')->where($data)->find();
			$this->assign('result',$result);
			$this->display();
		}else{
			$_POST['id'] = I('id');
			$_POST['news_info'] = I('news_info');
			if(!$news->create()){
				$this->error('数据过滤失败！');
			}else{
				$result = $news->save();
				if(!$result){
					$this->ajaxReturn(0);
				}else{
					$this->ajaxReturn(1);
				}
			}
		}
	}
	
    //编辑公告
    public function gonggao_edit(){
        if(!empty($_POST)){
            $m = M('page');
            $map['id']=3;   //公告ID
            $result = $m->where($map)->save($_POST);
            if($result){
                $this->success('修改成功');
            }else{
                $this->error('修改失败');
            }
        }else{
            $map['id']=3;
            $m = M('page');
            $result = $m->where($map)->find();
            $this->assign('result',$result);
            $this->display();
        }       
    }
}




