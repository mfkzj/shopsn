<?php
namespace Common\Controller;

use Think\Controller;
use Common\Tool\Tool;
use Common\Model\ConfigChildrenModel;
use Common\Model\SystemConfigModel;

trait CommonController 
{
    /**
     * 获取系统配置
     */
    public function getConfig($key = null)
    {
        if (!S('config'))
        {
            //获取字表数据
            $children    = ConfigChildrenModel::getInitnation()->getAllConfig();
            //获取配置值
            $configValue = SystemConfigModel::getInitnation()->getAllConfig();
            //组合数据
            Tool::connect('ArrayParse', array('children' => $children, 'configValue'=> $configValue));
            $receive = array();
            $data = Tool::buildConfig()->parseConfig()->oneArray($receive);
            S('config', $receive, 100);
        }
        $data = S('config'); 
        return $key === null ? $data : $data[$key];
    }
}