<?php
namespace API\Controller;
use API\Model\FootPrintModel;
use Common\Tool\Tool;

/**
 * 我的足迹 
 */

class MyFootprintController extends BaseController
{
    public function __construct()
    {
        parent::__construct();
    
        $this->isLogin();
    }
    
    // 我的足迹
    public function personFootprint()
    {
        $data =  FootPrintModel::getInitation()->getUserInfoById();
        
        $this->updateClient($data, '操作');
    }
    
    // 清空足迹
    public function emptyFootprint()
    {
        $status =  FootPrintModel::getInitation()->delete($_SESSION['userId'], 'uid');
        
        $this->updateClient($status, '清空', true);
    }
    
    // 删除单个足迹
    public function delFootprint()
    {
        Tool::checkPost($_POST, array('is_numeric' => array('id')), true , array('id')) ? true : $this->ajaxReturnData(null, '400', '参数错误');
        $status =  FootPrintModel::getInitation()->delete($_POST['id']);
        
        $this->updateClient($status, '删除');
    }
}