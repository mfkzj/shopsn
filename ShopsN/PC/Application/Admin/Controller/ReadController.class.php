<?php
/*
 * @thinkphp3.2.2  auth认证   php5.3以上
 * @Created on 2015/08/18
 * @Author  夏日不热(老屁)   757891022@qq.com
 *
 */
namespace Admin\Controller;
use Think\Controller;

class ReadController extends Controller
{
	//获取确认订单完成时间
	public function read_shouhuo_time(){
		if(IS_AJAX){
			$order=M('goods_orders');
			$id=I('id');
			$data=$order->where(array('id'=>$id))->find();
			$this->ajaxReturn(json_encode($data));
			
		}
	}
	//获取用户等级
	public function read_grade(){
		if(IS_AJAX){
			$order=M('member','vip_');
			$id=I('id');
			$data=$order->where(array('id'=>$id))->find();
			$this->ajaxReturn(json_encode($data));
			
		}
	}
	
}